const express=require('express');
const cors=require('cors');
require('./src/db/dbConnection.js');
const dotenv=require('dotenv');
const router=require('./src/routes/router.js');
const app = express();
app.use(cors());
dotenv.config();
app.use(router);
const PORT=parseInt(process.env.PORT) || 8000;
app.listen(PORT,()=>{
    console.log("server listening on port",PORT);
})