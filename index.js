const express=require('express');
const cors=require('cors');
const bodyParser = require('body-parser');
const dotenv=require('dotenv');
const app=express();
//this is for removing cors from ui.
app.use(cors());
// this is for use .env file
let result=dotenv.config();
if (result.error) {
    console.error('Error loading .env file:', result.error);
  }  
// this is for access data from request body
// Parse JSON bodies
app.use(bodyParser.json());
// Parse URL-encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));
//this is for run the backend server on port
const PORT=parseInt(process.env.PORT) || 8000;
let data=[
    {id:1,name:"Aman Kumar"}
];
// this is testing get api
app.get('/',(request,response)=>{
    response.send("Hello Express!");
})
app.get('/users',(request,response)=>{
    response.status(200).json({responseCode:0,responseDesc:'Success',users:data});
})
//this is for create user
app.post('/add/user',(request,response)=>{
    data.push(request.body);
    response.status(200).json({responseCode:0,responseDesc:"user created successfully"})
})
//this is for update user
app.put('/update/user/:id',(request,response)=>{
console.log(request.body);
let id=parseInt(request.params.id);
let name=request.body.name;
data.forEach((item)=>{
    if(item.id===id){
        item.name=name;
    }
})
response.status(200).json({responseCode:0,responseDesc:"user updated successfully",users:data});
})
//this is for delete user
app.delete('/delete/user/:id',(request,response)=>{
    let id=parseInt(request.params.id);
    data=data.filter(item=>item.id!==id);
response.status(200).json({responseCode:0,responseDesc:"user deleted successfully"});
})
// this is for run the backend
app.listen(PORT,()=>{
    console.log("Server listening on port",PORT);
});