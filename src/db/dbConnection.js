const mongoose = require('mongoose');
require('dotenv').config();
const DB=process.env.DATABASE;
console.log('Database', DB);
mongoose.connect(DB,{
    useUnifiedTopology: true,
    useNewUrlParser: true
}).then(()=>{
    console.log("Database connected successfully!");
}).catch((error)=>{
    console.log("Error connecting to database",error);
})
