const express=require('express');
const router=express.Router();
const userController=require('../controller/userController');
//routes
router.get('/',(request,response)=>{
    response.send("Hello Express!");
});
router.get('/getUsers',userController.getUsers);
module.exports=router;